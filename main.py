#!/usr/bin/env python

import time
import os
import random
from picamera import PiCamera
import easygopigo3 as easy

# distance in cm of a blocking obj before robot
# should turn
COLLISION_DISTANCE = 290

FILE_PATH = '/home/pi/Desktop/rover_logs/'

SLEEP_TIME = 2

gpg = easy.EasyGoPiGo3()

cam = PiCamera()

distance_sensor = gpg.init_distance_sensor()

folder_name = time.strftime('%Y %m %e', time.localtime(time.time()))

last_direction = 'fwd'

def set_eyes_default():
	gpg.set_left_eye_color((1, 255, 1))
	gpg.set_right_eye_color((1, 255, 1))
	gpg.led_on("left")
	gpg.led_on("right")

def set_eyes_turning(direction):
	if direction == 'left':
		gpg.set_left_eye_color((1, 255, 1))
		gpg.set_right_eye_color((1, 1, 1))
		gpg.led_on("left")
		gpg.led_off("right")		
	else: 
		gpg.set_left_eye_color((1, 1, 1))
		gpg.set_right_eye_color((1, 255, 1))
		gpg.led_off("left")
		gpg.led_on("right")		


# MAIN
print('Starting in 5 seconds...')
time.sleep(5)

if 'rover_logs' in os.listdir('/home/pi/Desktop/') == False:
	os.mkdir(FILE_PATH)

try: 
	while True:
		print(distance_sensor.read_mm())
		# if nothing is blocking, go straight
		if distance_sensor.read_mm() >= COLLISION_DISTANCE:
			set_eyes_default()
			gpg.forward()
			last_direction = 'fwd'

		# if potentially blocked, turn left or right
		else:
			gpg.stop()
			#filename = time.strftime('%H:%M:%S', time.localtime(time.time()))
			# check if subfolder exists, if not, create it
			#if folder_name in os.listdir(FILE_PATH) == False:
				#os.mkdir(FILE_PATH . folder_name . '/')
			#cam.capture(FILE_PATH . folder_name . file_name . '.jpg')
			# if last direction == fwd, turn left or right randomly
			if last_direction == 'fwd':
				if random.random() > 0.5:
					gpg.turn_degrees(-45)
					time.sleep(SLEEP_TIME)
					last_direction = 'left'	
				else:
					gpg.turn_degrees(45)
					time.sleep(SLEEP_TIME)
					last_direction = 'right'
			elif last_direction == 'left':
				gpg.turn_degrees(-45)
				time.sleep(SLEEP_TIME)
				last_direction = 'left'

			else:
				gpg.turn_degrees(45)
				time.sleep(SLEEP_TIME)
				last_direction = 'right'

except KeyboardInterrupt:
	# do clean up and write log files
	gpg.reset_all()
